Please setup all the required parameters in .env file

Create Database:
 - php bin/console doctrine:database:create

Execute Migrations:
 - php bin/console doctrine:migrations:migrate

Execute Fixtures 
 - php bin/console doctrine:fixtures:load
 
Start Server and go to /api uri