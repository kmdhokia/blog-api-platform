<?php

namespace App\Tests;

class UserTest extends BaseTestClass
{
    public function testRegister(): void
    {
        $params = [
            'email' => 'user6@example.com',
            'password' => 'user6',
            'firstname' => 'firstname6',
            'lastname' => 'lastname6',
        ];

        $response = $this->request('POST', '/api/register', null, $params);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $data = json_decode($response->getContent());

        $this->assertObjectHasAttribute('@context', $data);
        $this->assertObjectHasAttribute('@id', $data);
        $this->assertObjectHasAttribute('@type', $data);
        $this->assertEquals($params['email'], $data->email);
        $this->assertEquals($params['firstname'], $data->firstname);
        $this->assertEquals($params['lastname'], $data->lastname);
        $this->assertObjectNotHasAttribute('password', $data);
    }

    public function testRegisterFailed()
    {
        $params = [
            'email' => 'user6@example.com',
        ];

        $response = $this->request('POST', '/api/register', null, $params);
        $this->assertEquals(400, $response->getStatusCode());
    }
}