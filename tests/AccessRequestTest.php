<?php

namespace App\Tests;

use App\Entity\AccessRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AccessRequestTest extends BaseTestClass
{
    public function testAccessRequestGet(): void
    {
        $token = $this->getToken('admin@example.com', 'admin');
        $response = $this->request('GET', '/api/access_requests', $token, []);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $data = json_decode($response->getContent());
        $this->assertObjectHasAttribute('@context', $data);
        $this->assertObjectHasAttribute('@id', $data);
        $this->assertObjectHasAttribute('@type', $data);
        $accessRequests = $response->toArray()['hydra:member'];
        $this->assertCount(1, $accessRequests);
    }

    public function testAccessRequestsAccessDenied()
    {
        $token = $this->getToken('user1@example.com', 'user1');
        $this->request('GET', '/api/access_requests', $token, []);
        $this->assertResponseStatusCodeSame(403);
    }

    public function testAccessRequestPost()
    {
        $token = $this->getToken('user1@example.com', 'user1');
        $files = [
            'imageFile' => $this->getTestAsset(),
        ];
        $params = [
            'message' => 'please grant me access for blogger',
        ];

        $this->request('POST', '/api/access_requests', $token, $params, $files);
        $this->assertResponseStatusCodeSame(201);
    }

    public function testAccessRequestGrantPost()
    {
        $token = $this->getToken('admin@example.com', 'admin');
        $params = [
            'status' => AccessRequest::STATUS_APPROVED,
        ];

        $this->request('POST', '/api/access_requests/grant/1', $token, $params);
        $this->assertResponseStatusCodeSame(201);

        // user1 should be able to add blog post
        $token = $this->getToken('user1@example.com', 'user1');
        $date = new \DateTime();
        $params = [
            'title' => 'test title',
            'content' => 'test content',
            'date' => $date->format('Y-m-d H:i:s'),
        ];

        $this->request('POST', '/api/blog_posts', $token, $params);
        $this->assertResponseStatusCodeSame(201);
    }

    public function testAccessRequestGrantPostDenied()
    {
        $token = $this->getToken('admin@example.com', 'admin');
        $params = [
            'status' => AccessRequest::STATUS_DECLINED,
            'declineMessage' => 'No more bloggers',
        ];

        $this->request('POST', '/api/access_requests/grant/1', $token, $params);
        $this->assertResponseStatusCodeSame(201);
    }
}