<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BaseTestClass extends ApiTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass(): void
    {
        self::bootKernel();
    }

    /**
     * @param $method
     * @param $uri
     * @param null $token
     * @param array $params
     * @param array $files
     * @param array $servers
     * @return \ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Response|\Symfony\Contracts\HttpClient\ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function request($method, $uri, $token = null, $params = [], $files = [], $servers = [])
    {
        if ($token) {
            $parameters['headers'] = [
                "Authorization" => "BEARER {$token}",
            ];
        }
        $parameters['json'] = $params;
        if (!empty($files)) {
            $parameters['extra']['files'] = $files;
        }
        if (!empty($parameters)) {
            $parameters['extra']['parameters'] = $params;
        }

        return static::createClient()->request($method, $uri, $parameters);
    }

    /**
     * @param $email
     * @param $password
     * @return mixed
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function getToken($email, $password)
    {
        $response = static::createClient()->request('POST', '/login_check', ['json' => [
            'email' => $email,
            'password' => $password,
        ]]);
        $content = json_decode($response->getContent(), true);

        return $content['token'];
    }

    /**
     * @return \Symfony\Component\HttpKernel\KernelInterface
     */
    protected function getKernel()
    {
        $options = [
            'environment' => 'test',
        ];
        $kernel = $this->createKernel($options);
        $kernel->boot();

        return $kernel;
    }

    /**
     * @param string $filename
     *
     * @return UploadedFile
     */
    protected function getTestAsset($filename = 'test_image.png')
    {
        $fs = static::$kernel->getContainer()->get('filesystem');
        $path = $this->getKernel()->getProjectDir().'/public/images/';
        $media = 'test.jpg';
        $testFile = str_replace($media, $filename, ($path.$media));
        $fs->copy($path.$media, $testFile);

        return new UploadedFile($testFile, $filename, 'image/png');
    }
}