<?php

namespace App\Tests;

class BlogPostTest extends BaseTestClass
{
    public function testBlogPostsGet()
    {
        $token = $this->getToken('user1@example.com', 'user1');
        $params = [
            'title' => 'test title',
            'content' => 'test content',
        ];

        $this->request('GET', '/api/blog_posts', $token, $params);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testBlogPostsPostAccessDenied()
    {
        $token = $this->getToken('user1@example.com', 'user1');
        $date = new \DateTime();
        $params = [
            'title' => 'test title',
            'content' => 'test content',
            'date' => $date->format('Y-m-d H:i:s'),
        ];

        $this->request('POST', '/api/blog_posts', $token, $params);
        $this->assertResponseStatusCodeSame(403);
    }

    public function testBlogPostsPost()
    {
        $token = $this->getToken('user5@example.com', 'user5');
        $date = new \DateTime();
        $params = [
            'title' => 'test title',
            'content' => 'test content',
            'date' => $date->format('Y-m-d H:i:s'),
        ];

        $this->request('POST', '/api/blog_posts', $token, $params);
        $this->assertResponseStatusCodeSame(201);
    }

    public function testBlogPostsPostInvalidParameter()
    {
        $token = $this->getToken('user5@example.com', 'user5');
        $params = [
            'title' => 'test title',
        ];

        $this->request('POST', '/api/blog_posts', $token, $params);
        $this->assertResponseStatusCodeSame(400);
    }
}