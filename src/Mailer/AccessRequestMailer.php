<?php

namespace App\Mailer;

use App\Entity\User;

class AccessRequestMailer extends BaseMailer
{
    public function sendApprovalMessage(User $user, $status, $message)
    {
        $html = "Hello {$user->getFirstname()}, Your request is {$status}.";
        if ($message) {
            $html .= " Reason: {$message}";
        }

        $this->sendEmailMessage($html, "Access request {$status}", $user->getEmail());
    }
}