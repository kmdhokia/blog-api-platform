<?php

namespace App\Mailer;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class BaseMailer
{
    protected MailerInterface $mailer;
    protected string $fromEmail;

    /**
     * BaseMailer constructor.
     * @param MailerInterface $mailer
     * @param string $fromEmail
     */
    public function __construct(MailerInterface $mailer, string $fromEmail)
    {
        $this->mailer = $mailer;
        $this->fromEmail = $fromEmail;
    }

    /**
     * @param $html
     * @param $subject
     * @param $toEmail
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    protected function sendEmailMessage($html, $subject, $toEmail)
    {
        $message = (new Email())
            ->from($this->fromEmail)
            ->to($toEmail)
            ->subject($subject)
            ->html($html);

        $this->mailer->send($message);
    }
}