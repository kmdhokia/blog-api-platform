<?php

namespace App\Repository;

use App\Entity\AccessRequest;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccessRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccessRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccessRequest[]    findAll()
 * @method AccessRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccessRequestRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccessRequest::class);
    }
}
