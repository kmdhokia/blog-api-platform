<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\AccessRequest;
use App\Manager\AccessRequestManager;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AccessRequestDataPersister implements DataPersisterInterface
{
    private AccessRequestManager $accessRequestManager;

    public function __construct(AccessRequestManager $accessRequestManager)
    {
        $this->accessRequestManager = $accessRequestManager;
    }

    public function supports($data): bool
    {
        return $data instanceof AccessRequest;
    }

    public function persist($accessRequest)
    {
        $accessRequests = $this->accessRequestManager->getFindBy(['user' => $accessRequest->getUser(), 'status' => AccessRequest::STATUS_APPROVED]);

        if (!empty($accessRequests)) {
            throw new BadRequestHttpException('User already has role Blogger');
        }

        $this->accessRequestManager->save($accessRequest);
    }

    public function remove($data)
    {
        // TODO: Implement remove() method.
    }
}