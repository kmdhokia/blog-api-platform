<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\User;
use App\Manager\UserManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserDataPersister implements DataPersisterInterface
{
    private UserManager $userManager;

    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserManager $userManager, UserPasswordEncoderInterface $encoder)
    {
        $this->userManager = $userManager;
        $this->encoder = $encoder;
    }

    public function supports($data): bool
    {
        return $data instanceof User;
    }

    public function persist($user)
    {
        if ($user->getPassword()) {
            $user->setPassword(
                $this->encoder->encodePassword($user, $user->getPassword())
            );
            $user->eraseCredentials();
        }
        $user->setRoles(['ROLE_USER']);
        $this->userManager->save($user);
    }

    public function remove($data)
    {
        // TODO: Implement remove() method.
    }
}