<?php

namespace App\Controller;

use App\Entity\AccessRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class AccessRequestAction
{
    public function __invoke(Request $request, TokenStorageInterface $tokenStorage): AccessRequest
    {
        $uploadedFile = $request->files->get('imageFile');

        if (!$uploadedFile) {
            throw new BadRequestHttpException('image "file" is required');
        }

        $accessRequest = new AccessRequest();
        $accessRequest->imageFile = $uploadedFile;
        $accessRequest->setTimestamp(new \DateTime());
        $accessRequest->setUser($tokenStorage->getToken()->getUser());
        $accessRequest->setMessage($request->request->get('message'));

        return $accessRequest;
    }
}