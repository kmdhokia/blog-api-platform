<?php

namespace App\Controller;

use App\Entity\AccessRequest;
use App\Mailer\AccessRequestMailer;
use App\Manager\AccessRequestManager;
use App\Manager\UserManager;
use Symfony\Component\HttpFoundation\Request;

final class GrantAccessRequestAction
{
    public function __invoke(Request $request, AccessRequestManager $accessRequestManager, UserManager $userManager, AccessRequestMailer $mailer): AccessRequest
    {
        $accessRequest = $accessRequestManager->getFind($request->get('id'));
        $status = $request->request->get('status');
        if (!in_array($status, AccessRequest::STATUSES)) {
            throw new \InvalidArgumentException(sprintf('Status `%s` is not valid option. valid options are [%s]', $status, implode(', ', AccessRequest::STATUSES)));
        }

        $user = $accessRequest->getUser();
        if (AccessRequest::STATUS_APPROVED === $status) {
            $user->setRoles(['ROLE_BLOGGER']);
            $userManager->save($user);
        }
        $declineMessage = $request->request->get('declineMessage');
        $accessRequest->setStatus($status);
        $accessRequest->setDeclineMessage($declineMessage);

        $mailer->sendApprovalMessage($user, $status, $declineMessage);

        return $accessRequest;
    }
}