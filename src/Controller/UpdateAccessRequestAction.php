<?php

namespace App\Controller;

use App\Entity\AccessRequest;
use App\Manager\AccessRequestManager;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class UpdateAccessRequestAction
{
    public function __invoke(Request $request, AccessRequestManager $accessRequestManager): AccessRequest
    {
        $accessRequest = $accessRequestManager->getFind($request->get('id'));
        $uploadedFile = $request->files->get('imageFile');

        if (!$uploadedFile) {
            throw new BadRequestHttpException('image "file" is required');
        }

        $accessRequest->imageFile = $uploadedFile;
        $accessRequest->setTimestamp(new DateTime());
        $accessRequest->setMessage($request->request->get('message'));

        return $accessRequest;
    }
}