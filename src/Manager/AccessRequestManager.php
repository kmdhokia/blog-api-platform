<?php

namespace App\Manager;

use App\Repository\AccessRequestRepository;

class AccessRequestManager extends BaseManager
{
    /**
     * AccessRequestManager constructor.
     * @param AccessRequestRepository $repository
     */
    public function __construct(AccessRequestRepository $repository)
    {
        $this->repository = $repository;
    }
}