<?php

namespace App\Manager;

use App\Repository\BlogPostRepository;

class BlogPostManager extends BaseManager
{
    /**
     * UserManager constructor.
     * @param BlogPostRepository $repository
     */
    public function __construct(BlogPostRepository $repository)
    {
        $this->repository = $repository;
    }
}