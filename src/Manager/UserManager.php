<?php

namespace App\Manager;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserManager extends BaseManager
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * UserManager constructor.
     * @param UserRepository $repository
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserRepository $repository, UserPasswordEncoderInterface $encoder)
    {
        $this->repository = $repository;
        $this->encoder = $encoder;
    }

    /**
     * @param array $params
     * @return User
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(array $params): User
    {
        $user = new User();
        $user->setEmail($params['email']);
        $user->setPassword($this->encoder->encodePassword($user, $params['password']));
        $user->setFirstname($params['firstname']);
        $user->setLastname($params['lastname']);
        $user->setRoles(['ROLE_USER']);
        $this->save($user);

        return $user;
    }
}