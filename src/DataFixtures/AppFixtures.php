<?php

namespace App\DataFixtures;

use App\Entity\AccessRequest;
use App\Entity\BlogPost;
use App\Entity\User;
use App\Manager\UserManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;
    private UserManager $userManager;

    public function __construct(UserPasswordEncoderInterface $encoder, UserManager $userManager)
    {
        $this->encoder = $encoder;
        $this->userManager = $userManager;
    }

    public function load(ObjectManager $manager)
    {
        // add users
        $users = [
            ['admin@example.com', 'admin', ['ROLE_ADMIN'], 'firstname', 'lastname'],
            ['user1@example.com', 'user1', ['ROLE_USER'], 'firstname1', 'lastname1'],
            ['user2@example.com', 'user2', ['ROLE_USER'], 'firstname2', 'lastname2'],
            ['user3@example.com', 'user3', ['ROLE_USER'], 'firstname3', 'lastname3'],
            ['user4@example.com', 'user4', ['ROLE_USER'], 'firstname4', 'lastname4'],
            ['user5@example.com', 'user5', ['ROLE_BLOGGER'], 'firstname5', 'lastname5'],
        ];

        foreach ($users as $user) {
            $obj = new User();
            $obj->setEmail($user[0])
                ->setPassword($this->encoder->encodePassword($obj, $user[1]))
                ->setRoles($user[2])
                ->setFirstname($user[3])
                ->setLastname($user[4]);
            $this->userManager->save($obj);
        }

        // add blogposts
        $blogposts = [
            ['test title 1', 'test content 1', new \DateTime('-3 day')],
            ['test title 2', 'test content 2', new \DateTime('-2 day')],
            ['test title 3', 'test content 3', new \DateTime('-1 day')],
        ];
        $user5 = $this->userManager->getFindOneBy(['email' => 'user5@example.com']);

        foreach ($blogposts as $blogpost) {
            $obj = new BlogPost();
            $obj->setTitle($blogpost[0])
                ->setContent($blogpost[1])
                ->setDate($blogpost[2])
                ->setAuthor($user5);

            $manager->persist($obj);
        }

        // add access request
        $accessRequests = [
            [AccessRequest::STATUS_REQUESTED, new \DateTime('-4 day'), 'test.jpg']
        ];
        $user1 = $this->userManager->getFindOneBy(['email' => 'user1@example.com']);

        foreach ($accessRequests as $accessRequest) {
            $obj = new AccessRequest();
            $obj->setUser($user1)
                ->setStatus($accessRequest[0])
                ->setTimestamp($accessRequest[1])
                ->setImageName($accessRequest[2]);

            $manager->persist($obj);
        }

        $manager->flush();
    }
}
