<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\AccessRequestAction;
use App\Controller\GrantAccessRequestAction;
use App\Controller\UpdateAccessRequestAction;
use App\Repository\AccessRequestRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ApiResource(
 *      iri="http://schema.org/MediaObject",
 *      collectionOperations={
 *          "get"={"security"="is_granted('ROLE_ADMIN')","validation_groups"={"grant", "admin:read"},},
 *          "post"={
 *              "controller"=AccessRequestAction::class,
 *              "deserialize"=false,
 *              "security"="is_granted('ROLE_USER')",
 *              "validation_groups"={"read", "write"},
 *              "openapi_context"={
 *                  "requestBody"={
 *                      "content"={
 *                          "multipart/form-data"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "properties"={
 *                                      "imageFile"={
 *                                          "type"="string",
 *                                          "format"="binary"
 *                                      },
 *                                       "message"={
 *                                           "type"="string"
 *                                       }
 *                                  }
 *                              }
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *          "get"={"security"="is_granted('ROLE_ADMIN')"}
 *      },
 *      itemOperations={
 *          "get"={"security"="is_granted('ROLE_ADMIN')"},
 *          "post"={
 *              "method"="POST",
 *              "path"="/access_requests/{id}",
 *              "controller"=UpdateAccessRequestAction::class,
 *              "security"="is_granted('ROLE_USER') and object.getUser() == user",
 *              "deserialize"=false,
 *              "validation_groups"={"read", "write"},
 *              "openapi_context"={
 *                  "requestBody"={
 *                      "content"={
 *                          "multipart/form-data"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "properties"={
 *                                      "imageFile"={
 *                                          "type"="string",
 *                                          "format"="binary"
 *                                      },
 *                                      "message"={
 *                                          "type"="string"
 *                                      }
 *                                  }
 *                              }
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *          "put"={
 *              "method"="POST",
 *              "path"="/access_requests/grant/{id}",
 *              "controller"=GrantAccessRequestAction::class,
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "deserialize"=false,
 *              "validation_groups"={"admin:read","grant"},
 *              "openapi_context"={
 *                  "requestBody"={
 *                      "content"={
 *                          "multipart/form-data"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "properties"={
 *                                      "status"={
 *                                          "type"="string"
 *                                      },
 *                                      "declineMessage"={
 *                                          "type"="string"
 *                                      }
 *                                  }
 *                              }
 *                          }
 *                      }
 *                  }
 *              }
 *          }
 *      },
 *     normalizationContext={"groups"={"read", "grant", "admin:read"}},
 *     denormalizationContext={"groups"={"write", "grant", "admin:read"}}
 * )
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass=AccessRequestRepository::class)
 */
class AccessRequest
{
    const STATUS_APPROVED = 'approved';
    const STATUS_DECLINED = 'declined';
    const STATUS_REQUESTED = 'requested';
    const STATUSES = [
        self::STATUS_APPROVED,
        self::STATUS_DECLINED,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"admin:read", "grant"})
     * @Assert\NotBlank
     */
    private ?User $user;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank
     */
    private ?string $status = 'access_requested';

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     */
    private ?DateTimeInterface $timestamp;

    /**
     * @ORM\Column(type="string")
     * @Groups({"read"})
     * @Assert\NotBlank
     */
    protected ?string $imageName;

    /**
     * @Vich\UploadableField(mapping="image", fileNameProperty="imageName")
     * @Groups({"write"})
     */
    public ?File $imageFile = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"read", "write"})
     */
    private ?string $message = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"grant"})
     */
    private ?string $declineMessage = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTimestamp(): ?DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageName(?string $imageName): self
    {
        $this->imageName = $imageName;

        return $this;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageFile(?File $imageFile): self
    {
        $this->imageFile = $imageFile;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getDeclineMessage(): ?string
    {
        return $this->declineMessage;
    }

    public function setDeclineMessage(?string $declineMessage): self
    {
        $this->declineMessage = $declineMessage;

        return $this;
    }
}
