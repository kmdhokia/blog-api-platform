<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\BlogPost;
use App\Entity\User;
use App\Manager\BlogPostManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class AddAuthorSubscriber implements EventSubscriberInterface
{
    private TokenStorageInterface $tokenStorage;

    private BlogPostManager $blogPostManager;

    public function __construct(TokenStorageInterface $tokenStorage, BlogPostManager $blogPostManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->blogPostManager = $blogPostManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['addAuthor', EventPriorities::PRE_WRITE],
        ];
    }

    public function addAuthor(ViewEvent $event)
    {
        $blogPost = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$blogPost instanceof BlogPost || Request::METHOD_POST !== $method) {
            return;
        }

        $token = $this->tokenStorage->getToken();
        $author = $token->getUser();
        if (!$author instanceof User && $author->hasRoleBlogger()) {
            return;
        }

        $blogPost->setAuthor($author);
        $this->blogPostManager->save($blogPost);
    }
}